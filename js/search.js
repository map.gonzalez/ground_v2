const searchButton = document.querySelector('button[type="submit"]');
const searchInput = document.querySelector('input[type="search"]');

searchButton.addEventListener('click', search-input);
searchInput.addEventListener('input', search-input);


function search(event) {
    event.preventDefault();
  
    const searchTerm = searchInput.value.toLowerCase();
    const elements = document.querySelectorAll('.searchable-element');
  
    elements.forEach(element => {
      const text = element.textContent.toLowerCase();
      if (text.includes(searchTerm)) {
        element.style.display = 'block';
      } else {
        element.style.display = 'none';
      }
    });
  }