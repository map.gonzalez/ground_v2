const loginForm = document.querySelector('#loginForm')
loginForm.addEventListener('submit',(e)=>{
    e.preventDefault()
    const email = document.querySelector('#email').value
    const password = document.querySelector('#password').value
    const Users = JSON.parse(localStorage.getItem('users')) || []
    const userLog = Users.find(user => user.email === email && user.password === password)
    if(!userLog){
        return alert('Usuario y/o contraseña incorrecta')
    }
    localStorage.setItem("userLog",JSON.stringify(userLog));
    window.location.href = 'index.html' ;
    event.preventDefault();
    event.stopPropagation();
});

//e.preventDefault Cancela el evento si este es cancelable, 
// sin detener el resto del funcionamiento del evento, es decir, puede ser llamado de nuevo.

// addEventlistener, es un escuchador que indica al navegador que este atento a la interacción del usuario. 
//La ventaja es que se escribe totalmente en el JS, sin necesidad de tocar el HTML.

// querySelector() Devuelve el primer elemento del documento (utilizando un recorrido primero en profundidad 
// pre ordenado de los nodos del documento) que coincida con el grupo especificado de selectores.

// Con JSON.stringify podemos convertir un objeto en un string y lo inverso con JSON.parse.


// otra forma de validar correo y clave

// var form = document.getElementById("loginForm");
// form.addEventListener("submit", function (event) {
//   var email = document.getElementById("email").value;
//   var password = document.getElementById("password").value;

//   //validacion.
//   var users = JSON.parse(localStorage.getItem("users"));
//   const userLog = users.find((user) => user.email === email && user.password === password);
//   if(!userLog.email) {
//   return alert ('usuario o contraseña incorrecto');
//   }
  
//   localStorage.setItem("userLog",JSON.stringify(userLog));
//            window.location.href = "index.html";

//   event.preventDefault();
//   event.stopPropagation();
// });